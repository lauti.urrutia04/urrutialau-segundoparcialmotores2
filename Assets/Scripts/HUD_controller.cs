using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUD_controller : MonoBehaviour
{
    public TMPro.TMP_Text winloseText;
    public TMPro.TMP_Text objectiveText;
    public List<string> objectives = new List<string>();
    void Start()
    {
        //objectiveText.gameObject.SetActive(true);
        winloseText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            winloseText.text = "You Lost";
            winloseText.gameObject.SetActive(true);
            //objectiveText.gameObject.SetActive(false);
        }
        if (GameManager.gameWon)
        {
            Time.timeScale = 0;
            winloseText.text = "You Won!";
            winloseText.gameObject.SetActive(true);
            //objectiveText.gameObject.SetActive(false);
        }
        if (GameManager.gamePaused)
        {
            Time.timeScale = 0;
            winloseText.text = "Game Paused";
            winloseText.gameObject.SetActive(true);
            //objectiveText.gameObject.SetActive(false);
        }

        if (GameManager.currentPlayer == 0)
        {
            //objectiveText.text = "Desbloquea y atraviesa el portal \nSaltando";
            objectiveText.text = objectives[0];
        }
        if (GameManager.currentPlayer == 1)
        {
            //objectiveText.text = "Desbloquea y atraviesa el portal \nDeslizandote";
            objectiveText.text = objectives[1];
        }
        if (GameManager.currentPlayer == 2)
        {
            //objectiveText.text = "Evita a los enemigos y atraviesa el portal";
            objectiveText.text = objectives[2];
        }
    }
}
