using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Lift : MonoBehaviour
{
    public bool isPlayerOn = false;
    public float speed = 10;
    public float maxY = 30;
    public float minY = 20;
    void Start()
    {
        
    }

    public virtual void Update()
    {
        if (isPlayerOn)
        {
            if (transform.position.y <= maxY)
            {
                MoveUp();
            }
        }
        else
        {
            if (transform.position.y >= minY)
            {
                MoveDown();
            }
        }
    }

    public void MoveUp()
    {
        transform.position += transform.up * speed * Time.deltaTime;
    }
    public void MoveDown()
    {
        transform.position -= transform.up * speed * Time.deltaTime;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerOn = true;
        }
    }
    public virtual void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerOn = false;
        }
    }
}
