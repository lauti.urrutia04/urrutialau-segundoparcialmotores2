using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_AntiLift : Controller_Lift
{

    public override void Update()
    {
        if (isPlayerOn)
        {
            if (transform.position.y >= minY)
            {
                MoveDown();
            }
        }
        else
        {
            if (transform.position.y <= maxY)
            {
                MoveUp();
            }
        }
    }
}
